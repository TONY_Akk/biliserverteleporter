package com.bilibili.serverteleporter.api.events

import com.bilibili.serverteleporter.misc.PortalData
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

/**
 * 在玩家激活传送门结构并设置ID后出触发该事件.
 *
 * @param player 激活传送门的玩家
 * @param portalData 传送门对象
 */
@Suppress("MemberVisibilityCanBePrivate")
class PortalPostCreateEvent(val player: Player, val portalData: PortalData) : Event() {
    override fun getHandlers(): HandlerList = handlerList

    companion object {
        @JvmStatic
        val handlerList = HandlerList()
    }
}