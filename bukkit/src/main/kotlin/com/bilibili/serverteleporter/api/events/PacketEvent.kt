package com.bilibili.serverteleporter.api.events

import com.bilibili.serverteleporter.network.CustomPacket
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

open class PacketEvent(val packet: CustomPacket) : Event(true) {
    override fun getHandlers(): HandlerList = handlerList

    companion object {
        @JvmStatic
        val handlerList = HandlerList()
    }
}