package com.bilibili.serverteleporter.api.events

import com.bilibili.serverteleporter.misc.PortalCalculator
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

/**
 * 在玩家建立传送门结构并尝试激活时出触发该事件.
 *
 * @param player 激活传送门的玩家
 * @param portal 传送门生成器对象
 */
@Suppress("MemberVisibilityCanBePrivate")
class PortalPreCreateEvent(val player: Player, val portal: PortalCalculator) : Event(), Cancellable {
    private var isCancelled = false

    override fun getHandlers(): HandlerList = handlerList

    override fun isCancelled(): Boolean = isCancelled

    override fun setCancelled(cancel: Boolean) {
        isCancelled = cancel
    }

    companion object {
        @JvmStatic
        val handlerList = HandlerList()
    }
}