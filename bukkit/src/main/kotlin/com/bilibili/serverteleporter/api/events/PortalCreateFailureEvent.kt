package com.bilibili.serverteleporter.api.events

import com.bilibili.serverteleporter.network.CustomPacket
import org.bukkit.event.HandlerList

class PortalCreateFailureEvent(packet: CustomPacket) : PacketEvent(packet) {
    override fun getHandlers(): HandlerList = handlerList

    companion object {
        @JvmStatic
        val handlerList = HandlerList()
    }
}