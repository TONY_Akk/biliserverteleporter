package com.bilibili.serverteleporter.settings

import com.bilibili.serverteleporter.ServerTeleporterBukkit
import com.bilibili.serverteleporter.logger
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.yaml.snakeyaml.error.YAMLException
import java.io.File

object BukkitSettings {
    private val settingsFile = File(ServerTeleporterBukkit.instance.dataFolder, "settings.yml").apply {
        if (!exists()) {
            parentFile.mkdirs()
            ServerTeleporterBukkit.instance.saveResource("settings.yml", false)
        }
    }
    lateinit var settings: FileConfiguration
        private set

    init {
        try {
            settings = YamlConfiguration.loadConfiguration(settingsFile)
        } catch (e: YAMLException) {
            logger.error("§c加载配置文件 ${settingsFile.path} 时出现错误: §b${e.message}")
        }
    }
}