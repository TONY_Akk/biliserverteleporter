package com.bilibili.serverteleporter.storage

import com.bilibili.serverteleporter.misc.PortalData

interface IPortalStorage {
    fun storagePortal(portal: PortalData)

    fun removePortal(portal: PortalData)

    fun loadPortals(): List<PortalData>
}