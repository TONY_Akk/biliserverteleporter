package com.bilibili.serverteleporter.storage

import com.bilibili.serverteleporter.ServerTeleporterBukkit
import com.bilibili.serverteleporter.enums.Axis
import com.bilibili.serverteleporter.logger
import com.bilibili.serverteleporter.misc.PortalData
import org.bukkit.Location
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.yaml.snakeyaml.error.YAMLException
import java.io.File

object YamlStorage : IPortalStorage {
    private lateinit var storageConfig: FileConfiguration
    private val storageFile = File(ServerTeleporterBukkit.instance.dataFolder, "storage.yml").apply {
        if (!this.exists()) {
            parentFile.mkdirs()
            createNewFile()
        }
    }

    init {
        logger.info("§b正在创建数据文件...")
        try {
            val storage = YamlConfiguration.loadConfiguration(storageFile)
            storageConfig = storage
        } catch (e: YAMLException) {
            logger.error("§c加载配置文件 ${storageFile.path} 时出现错误: §b${e.message}")
        }
    }

    override fun storagePortal(portal: PortalData) {
        try {
            storageConfig["storagePortals"] = storageConfig.getMapList("storagePortals").also {
                it.add(mapOf(
                    "creator" to portal.creator,
                    "id" to portal.id,
                    "axis" to portal.axis.customName,
                    "height" to portal.height,
                    "width" to portal.width,
                    "corner" to portal.bottomLeft
                ))
            }
            storageConfig.save(storageFile)
        } catch (e: IllegalStateException) {
            logger.error("§c由于配置文件未加载, 未能保存传送门数据.")
        }
    }

    override fun removePortal(portal: PortalData) {
        try {
            storageConfig["storagePortals"] = storageConfig.getMapList("storagePortals").apply {
                removeIf {
                    it["id"] == portal.id
                }
            }
            storageConfig.save(storageFile)
        } catch (e: IllegalStateException) {
            logger.error("§c由于配置文件未加载, 未能保存传送门数据.")
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun loadPortals(): List<PortalData> {
        try {
            return storageConfig.getMapList("storagePortals").map {
                val creator = it["creator"] as String
                val id = it["id"] as Int
                val axis = Axis.byName(it["axis"] as String) ?: return@map logger.error("无法读取传送门方向, 已跳过")
                val height = it["height"] as Int
                val width = it["width"] as Int
                val bottomLeft = it["corner"] as Location
                PortalData(creator, id, bottomLeft, axis, height, width)
            }.filterIsInstance<PortalData>()
        } catch (e: Exception) {
            logger.error("§c由于配置文件未加载, 未能保存传送门数据.")
            return emptyList()
        }
    }
}