package com.bilibili.serverteleporter.utils

import com.bilibili.serverteleporter.ServerTeleporterBukkit
import kotlinx.coroutines.asCoroutineDispatcher
import org.bukkit.Bukkit
import java.util.concurrent.Executor

//suspend fun sync() = suspendCoroutine<Unit> {
//    Bukkit.getScheduler().runTask(ServerTeleporterBukkit.instance, Runnable {
//        it.resume(Unit)
//    })
//}
//
//suspend fun async() = suspendCoroutine<Unit> {
//    Bukkit.getScheduler().runTaskAsynchronously(ServerTeleporterBukkit.instance, Runnable {
//        it.resume(Unit)
//    })
//}

val bukkitSyncContext = Executor {
    Bukkit.getScheduler().runTask(ServerTeleporterBukkit.instance, it)
}.asCoroutineDispatcher()

val bukkitAsyncContext = Executor {
    Bukkit.getScheduler().runTaskAsynchronously(ServerTeleporterBukkit.instance, it)
}.asCoroutineDispatcher()