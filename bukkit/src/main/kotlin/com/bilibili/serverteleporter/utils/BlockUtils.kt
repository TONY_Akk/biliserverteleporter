package com.bilibili.serverteleporter.utils

import org.bukkit.Material.*
import org.bukkit.block.Block

object BlockUtils {
    fun isPreciousBlock(block: Block) = when (block.type) {
        COPPER_BLOCK,
        CRAFTING_TABLE, IRON_BLOCK,
        GOLD_BLOCK, DIAMOND_BLOCK,
        EMERALD_BLOCK, NETHERITE_BLOCK,
        LAVA, CHEST,
        -> true
        else -> false
    }
}