package com.bilibili.serverteleporter.utils

import com.bilibili.serverteleporter.enums.Axis
import com.google.common.collect.AbstractIterator
import org.bukkit.util.Vector

object MathUtils {
    @Suppress("MemberVisibilityCanBePrivate")
    fun betweenClosed(x1: Int, y1: Int, z1: Int, x2: Int, y2: Int, z2: Int): Iterable<Vector> {

        val offsetX = x2 - x1 + 1
        val offsetY = y2 - y1 + 1
        val offsetZ = z2 - z1 + 1
        val offsetProduct = offsetX * offsetY * offsetZ

        return Iterable {
            object : AbstractIterator<Vector>() {
                private val cursor: Vector = Vector()
                private var index = 0

                override fun computeNext(): Vector? {
                    return if (index == offsetProduct) {
                        endOfData()
                    } else {
                        val i1 = index % offsetX
                        val j1 = index / offsetX
                        val k1 = j1 % offsetY
                        val l1 = j1 / offsetY
                        index++
                        cursor.apply {
                            x = (x1 + i1).toDouble()
                            y = (y1 + k1).toDouble()
                            z = (z1 + l1).toDouble()
                        }
                    }
                }
            }
        }
    }

    fun betweenClosed(loc1: Vector, loc2: Vector): Iterable<Vector> {
        return betweenClosed(
            loc1.blockX, loc1.blockY, loc1.blockZ,
            loc2.blockX, loc2.blockY, loc2.blockZ
        )
    }

    fun sideOfRectangle(loc1: Vector, loc2: Vector, axis: Axis): List<Vector> {
        val min = Vector(minOf(loc1.x, loc2.x), minOf(loc1.y, loc2.y), minOf(loc1.z, loc2.z))
        val max = Vector(maxOf(loc1.x, loc2.x), maxOf(loc1.y, loc2.y), maxOf(loc1.z, loc2.z))

        val diffHorizontal = (axis.choose(max) - axis.choose(min)).toInt()
        val diffVertical = (max.y - min.y).toInt()

        val cache = HashSet<Vector>()

        repeat(diffHorizontal + 1) { horizon ->
            if (horizon == 0 || horizon == diffHorizontal) {
                repeat(diffVertical + 1) { vertica ->
                    cache.add(min.clone().add(axis.toVector().multiply(horizon))
                        .add(Vector(0.0, vertica.toDouble(), 0.0)))
                }
            }
            cache.add(min.clone().add(axis.toVector().multiply(horizon)))
            cache.add(max.clone().add(axis.toVector().multiply(-horizon)))
        }
        return cache.toList()
    }
}