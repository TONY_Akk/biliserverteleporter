package com.bilibili.serverteleporter.nms

import com.bilibili.serverteleporter.logger
import io.netty.channel.ChannelDuplexHandler
import io.netty.channel.ChannelHandlerContext
import net.minecraft.network.protocol.game.PacketPlayInUpdateSign
import net.minecraft.server.MinecraftServer
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent

object NMSListener : Listener {

    @EventHandler
    fun onQuit(quitEvent: PlayerQuitEvent) {
        SignSender.leavePlayer(quitEvent.player)
    }

    @EventHandler
    fun onJoin(loginEvent: PlayerJoinEvent) {
        val entityPlayer = MinecraftServer.getServer().playerList.getPlayer(loginEvent.player.uniqueId)!!
        entityPlayer.b?.a?.k?.pipeline()?.addBefore("packet_handler", "stper_handler", object : ChannelDuplexHandler() {
            override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
                if (msg is PacketPlayInUpdateSign) {
                    SignSender.receiveSign(loginEvent.player, msg.c().toList())
                }
                super.channelRead(ctx, msg)
            }
        }) ?: return logger.error("玩家 ${loginEvent.player.name} (${entityPlayer.uniqueID}) 不存在?")
    }

    @EventHandler
    fun onChant(chatEvent: AsyncPlayerChatEvent) {
        chatEvent.isCancelled = SignSender.receiveChat(chatEvent.player, chatEvent.message)
    }

}