package com.bilibili.serverteleporter.nms

import com.bilibili.serverteleporter.logger
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.chat.ComponentSerializer
import net.minecraft.core.BlockPosition
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.protocol.game.PacketPlayOutOpenSignEditor
import net.minecraft.network.protocol.game.PacketPlayOutTileEntityData
import net.minecraft.server.MinecraftServer
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

object SignSender {
    private val cache = ConcurrentHashMap<UUID, Continuation<List<String>>>()
    private val cacheAsk = ConcurrentHashMap<UUID, Continuation<Boolean>>()

    suspend fun Player.sendSign(default: List<String>): List<String> = suspendCoroutine {
        val location = location
        val defaultCopy = default.toMutableList()
        while (defaultCopy.size < 4) {
            defaultCopy.add("")
        }
        val signLoc = location.also { loc -> loc.y = loc.world?.minHeight?.toDouble() ?: return@also }
        sendBlockChange(signLoc, Bukkit.createBlockData(Material.OAK_SIGN))
        val pos = BlockPosition(signLoc.blockX, signLoc.blockY, signLoc.blockZ)
        val packetPlayOutUpdateSign = PacketPlayOutTileEntityData(pos, 9, NBTTagCompound().apply {
            setString("id", "minecraft:sign")
            setBoolean("keepPacked", false)
            setInt("x", pos.x)
            setInt("y", pos.y)
            setInt("z", pos.z)
            repeat(4) { count ->
                setString("Text${count + 1}", ComponentSerializer.toString(TextComponent(defaultCopy[count])))
            }
            setString("Color", "black")
        })
        val packetPlayOutOpenSignEditor = PacketPlayOutOpenSignEditor(pos)
        val entityPlayer = MinecraftServer.getServer().playerList.getPlayer(uniqueId)
        entityPlayer?.b?.sendPacket(packetPlayOutUpdateSign) ?: logger.error("玩家不存在, 无法发送告示牌")
        entityPlayer?.b?.sendPacket(packetPlayOutOpenSignEditor) ?: logger.error("玩家不存在, 无法发送告示牌")
        if (cache.containsKey(uniqueId)) {
            cache.remove(uniqueId)
            cache[uniqueId]!!.resumeWithException(IllegalStateException("Sign had not received before next send."))
        }
        cache[uniqueId] = it
    }

    suspend fun Player.askContinue(): Boolean = suspendCoroutine {
        sendMessage("§b| §f输入 §7[§bY§7] §f继续, 其他取消")
        if (cacheAsk.containsKey(uniqueId)) {
            cacheAsk.remove(uniqueId)
            cacheAsk[uniqueId]!!.resumeWithException(IllegalStateException("Message had not received before next send."))
        }
        cacheAsk[uniqueId] = it
    }

    fun receiveChat(player: Player, msg: String): Boolean {
        val continuation = cacheAsk[player.uniqueId] ?: return false
        continuation.resume(msg == "Y")
        cacheAsk.remove(player.uniqueId)
        return true
    }

    fun receiveSign(player: Player, content: List<String>) {
        val continuation = cache[player.uniqueId] ?: return
        continuation.resume(content)
        cache.remove(player.uniqueId)
    }

    fun leavePlayer(player: Player) {
        val continuation = cache[player.uniqueId] ?: return
        continuation.resume(emptyList())
        cache.remove(player.uniqueId)
    }
}