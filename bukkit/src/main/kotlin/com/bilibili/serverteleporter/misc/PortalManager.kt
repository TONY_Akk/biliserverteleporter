package com.bilibili.serverteleporter.misc

import com.bilibili.serverteleporter.dataStorage
import java.util.concurrent.CopyOnWriteArrayList

object PortalManager {
    private val loadedPortals = CopyOnWriteArrayList<PortalData>()
    private val portalCache = HashMap<Int, PortalData>()

    init {
        loadedPortals += dataStorage.loadPortals()
        loadedPortals.forEach { portalCache[it.id] = it }
    }

    fun getPortalByID(id: Int): PortalData? {
        return portalCache[id]
    }

    fun getLoadedPortals(): List<PortalData> {
        return ArrayList(loadedPortals)
    }

    fun addPortal(portalData: PortalData) {
        loadedPortals.add(portalData)
        portalCache[portalData.id] = portalData
        dataStorage.storagePortal(portalData)
    }

    fun removePortal(portalData: PortalData) {
        portalCache.remove(portalData.id)
        loadedPortals.remove(portalData)
        dataStorage.removePortal(portalData)
    }
}