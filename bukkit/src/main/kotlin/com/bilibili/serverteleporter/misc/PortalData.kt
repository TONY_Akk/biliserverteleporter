package com.bilibili.serverteleporter.misc

import com.bilibili.serverteleporter.enums.Axis
import com.bilibili.serverteleporter.logger
import com.bilibili.serverteleporter.utils.MathUtils
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.BlockFace
import org.bukkit.util.Vector
import kotlin.math.max
import kotlin.math.min

data class PortalData constructor(
    val creator: String,
    val id: Int,
    val bottomLeft: Location,
    val axis: Axis,
    val height: Int,
    val width: Int,
) {
    private val rightDirection = if (axis == Axis.X) BlockFace.WEST else BlockFace.SOUTH
    private val calculator by lazy { PortalCalculator.findPortalShape(bottomLeft, axis) { it.isValid() } }

    val safeLoc: Location = bottomLeft.clone()
        .add(0.0, 1.0, 0.0)
        .add(rightDirection.direction)

    fun createPortal() {
        logger.verbose("§b正在生成传送门")
        calculator.ifPresent {
            it.createPortalBlocks()
        }
    }

    fun createFrame() {
        logger.verbose("§b正在生成传送门框架, 位置: ${bottomLeft.toVector()}")
        val sideOfRectangle = calculateFrame()

        sideOfRectangle.forEach {
            val toLocation =
                it.toLocation(bottomLeft.world ?: throw IllegalStateException("World of location cannot be null"))
            toLocation.block.type = Material.OAK_LOG
        }

        for (vector in MathUtils.betweenClosed(bottomLeft.toVector(),
            bottomLeft.clone().add(0.0, (height - 1).toDouble(), 0.0).add(rightDirection.direction.multiply(width - 1))
                .toVector())) {
            val toLocation = vector.toLocation(bottomLeft.world
                ?: throw IllegalStateException("World of location cannot be null"))
            toLocation.block.type = Material.AIR
        }

    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun getNecessaryFrame(): Set<Vector> {
        val (bottomLeftCorner, topRightCorner) = calculateCorner()
        val bottomRightCorner = bottomLeftCorner.clone().add(rightDirection.direction.multiply(width + 1))
        val topLeftCorner = topRightCorner.clone().add(rightDirection.oppositeFace.direction.multiply(width + 1))
        val frameBlocks = calculateFrame().toMutableList()
        frameBlocks.removeAll(listOf(bottomLeftCorner.toVector(),
            topRightCorner.toVector(),
            bottomRightCorner.toVector(),
            topLeftCorner.toVector()))
        return frameBlocks.toSet()
    }

    fun getNecessaryBlocks(): Set<Vector> {
        return getPortalBlocks() + getNecessaryFrame()
    }

    fun getPortalBlocks(): Set<Vector> {
        val pos1 = bottomLeft.toVector()
        val pos2 =
            bottomLeft.clone()
                .add(0.0, (height - 1).toDouble(), 0.0)
                .add(rightDirection.direction.multiply(width - 1))
                .toVector()
        val min = Vector(min(pos1.x, pos2.x), min(pos1.y, pos2.y), min(pos1.z, pos2.z))
        val max = Vector(max(pos1.x, pos2.x), max(pos1.y, pos2.y), max(pos1.z, pos2.z))
        val betweenClosed = MathUtils.betweenClosed(min, max)
        return betweenClosed.map { it.clone() }.toSet()
    }

    fun isSafeToCreate(): Boolean {
        val rightDirection = if (axis == Axis.X) BlockFace.WEST else BlockFace.SOUTH

        for (vector in MathUtils.betweenClosed(bottomLeft.toVector(),
            bottomLeft.add(0.0, (height - 1).toDouble(), 0.0).add(rightDirection.direction.multiply(width - 1))
                .toVector())) {
            val toLocation =
                vector.toLocation(bottomLeft.world ?: throw IllegalStateException("World of location cannot be null"))
            if (!toLocation.block.type.isAir) {
                return false
            }
        }
        return true
    }

    private fun calculateFrame(): List<Vector> {
        val (bottomLeftCorner, topRightCorner) = calculateCorner()
        return MathUtils.sideOfRectangle(bottomLeftCorner.toVector(), topRightCorner.toVector(), axis)
    }

    private fun calculateCorner(): Pair<Location, Location> {
        val bottomLeftCorner = bottomLeft.clone()
            .add(0.0, -1.0, 0.0)
            .add(rightDirection.oppositeFace.direction)
        val topRightCorner = bottomLeft.clone()
            .add(0.0, height.toDouble(), 0.0)
            .add(rightDirection.direction.multiply(width))
        return bottomLeftCorner to topRightCorner
    }

}
