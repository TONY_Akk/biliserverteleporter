package com.bilibili.serverteleporter.misc

import com.bilibili.serverteleporter.enums.Axis
import com.bilibili.serverteleporter.utils.MathUtils
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.block.data.Orientable
import org.bukkit.util.Vector
import java.util.*
import java.util.function.Predicate
import kotlin.math.max
import kotlin.math.min

class PortalCalculator private constructor(loc: Location, val axis: Axis) {
    private val rightDirection =
        if (axis == Axis.Y) throw IllegalArgumentException("Portal cannot create in this side") else {
            if (axis == Axis.X) {
                BlockFace.WEST
            } else {
                BlockFace.SOUTH
            }
        }
    var width = 0
        private set
    var height = 0
        private set
    var bottomLeft: Location
        private set
    private var numPortalBlocks = 0

    init {
        val tempLoc = calculateBottomLeft(loc)
        if (tempLoc != null) {
            bottomLeft = tempLoc
            width = calculateWidth()
            if (width > 0) {
                height = calculateHeight()
            }
        } else {
            bottomLeft = loc
            width = 1
            height = 1
        }
    }

    private fun calculateBottomLeft(loc: Location): Location? {
        val location = loc.clone()
        // 获取当前 x, y下最靠下的传送门方块 (不包括黑曜石)
        val world = location.world ?: throw IllegalStateException("World of location cannot be null")
        val height = maxOf(world.minHeight, location.blockY - MAX_HEIGHT)
        while (location.blockY > height && isEmpty(location.clone().apply { y-- }.block)) {
            location.y--
        }

        val face: BlockFace = rightDirection.oppositeFace
        // 获取 X/Y (E/W方向) 靠近黑曜石的位置
        val distance = getDistanceUntilEdgeAboveFrame(location, face) - 1
        // 返回传送门角落的方块 (不包括黑曜石)
        return if (distance < 0) null else location.add(face.direction.multiply(distance))
    }

    private fun getDistanceUntilEdgeAboveFrame(loc: Location, face: BlockFace): Int {
        var location: Location
        for (i in 0..MAX_WIDTH) {
            location = loc.clone()
            location.add(face.direction.multiply(i))
            var block = location.block
            if (!isEmpty(block)) {
                if (testFrame(block)) {
                    return i
                }
                break
            }

            block = location.add(0.0, -1.0, 0.0).block
            if (!testFrame(block)) {
                break
            }
        }
        return 0
    }

    private fun calculateWidth(): Int {
        val i = getDistanceUntilEdgeAboveFrame(bottomLeft, rightDirection)
        return if (i in MIN_WIDTH..MAX_WIDTH) i else 0
    }

    private fun calculateHeight(): Int {
        val i: Int = this.getDistanceUntilTop()
        return if (i in MIN_HEIGHT..MAX_HEIGHT && this.hasTopFrame(i)) i else 0
    }

    private fun hasTopFrame(height: Int): Boolean {
        for (i in 0 until width) {
            val location = bottomLeft.clone()
                .add(0.0, height.toDouble(), 0.0)
                .add(rightDirection.direction.multiply(i))
            if (!testFrame(location.block)) {
                return false
            }
        }
        return true
    }

    private fun getDistanceUntilTop(): Int {
        var location: Location
        for (i in 0 until MAX_HEIGHT) {
            location = bottomLeft.clone()
                .add(0.0, i.toDouble(), 0.0)
                .add(rightDirection.direction.multiply(-1))
            if (!testFrame(location.block)) {
                return i
            }

            location = bottomLeft.clone()
                .add(0.0, i.toDouble(), 0.0)
                .add(rightDirection.direction.multiply(width))
            if (!testFrame(location.block)) {
                return i
            }

            for (j in 0 until width) {
                location = bottomLeft.clone()
                    .add(0.0, i.toDouble(), 0.0)
                    .add(rightDirection.direction.multiply(j))
                val block = location.block
                if (!isEmpty(block)) {
                    return i
                }
                if (block.type == Material.NETHER_PORTAL) {
                    this.numPortalBlocks++
                }
            }
        }
        return MAX_HEIGHT
    }

    /**
     * 判断 传送门尺寸是否合法
     *
     * @return 如果大小满足要求, 则返回 true 否则 false
     */
    fun isValid(): Boolean = width in MIN_WIDTH..MAX_WIDTH && height in MIN_HEIGHT..MAX_HEIGHT

    /**
     * 判断 传送门结构是否已形成
     *
     * @return 如果已经形成传送门结构, 则返回 true 否则 false
     */
    fun isComplete(): Boolean {
        return isValid() && numPortalBlocks == width * height
    }

    fun createPortalBlocks() {
        val pos1 = bottomLeft.toVector()
        val pos2 =
            bottomLeft.clone().add(0.0, (height - 1).toDouble(), 0.0).add(rightDirection.direction.multiply(width - 1))
                .toVector()
        MathUtils.betweenClosed(
            Vector(min(pos1.x, pos2.x), min(pos1.y, pos2.y), min(pos1.z, pos2.z)),
            Vector(max(pos1.x, pos2.x), max(pos1.y, pos2.y), max(pos1.z, pos2.z))
        ).forEach { vector ->
            val loc = vector.toLocation(bottomLeft.world ?: throw IllegalStateException("World won't be null?"))
            loc.block.apply {
                type = Material.NETHER_PORTAL
                val createBlockData = Bukkit.createBlockData(Material.NETHER_PORTAL) as Orientable
                blockData = createBlockData.also { it.axis = axis.toBukkit() }
            }
        }
    }

    companion object {
        const val MAX_HEIGHT = 21
        const val MAX_WIDTH = 21
        const val MIN_HEIGHT = 3
        const val MIN_WIDTH = 2
        private val testFrame = { block: Block -> block.type.name.contains("LOG") }

        private fun isEmpty(block: Block): Boolean {
            return block.type.isAir || (block.type == Material.FIRE) || (block.type == Material.NETHER_PORTAL)
        }

        fun findEmptyPortalShape(
            location: Location,
            axis: Axis,
        ): Optional<PortalCalculator> {
            return findPortalShape(location, axis) { portalShape: PortalCalculator ->
                portalShape.isValid() && portalShape.numPortalBlocks == 0
            }
        }

        @Suppress("MemberVisibilityCanBePrivate")
        fun findPortalShape(
            blockPos: Location,
            axis: Axis,
            portalShapePredicate: Predicate<PortalCalculator>,
        ): Optional<PortalCalculator> {
            val optional: Optional<PortalCalculator> =
                Optional.of(PortalCalculator(blockPos, axis))
                    .filter(portalShapePredicate)
            return if (optional.isPresent) {
                optional
            } else {
                val axis1: Axis = if (axis == Axis.X) Axis.Z else Axis.X
                Optional.of(PortalCalculator(blockPos, axis1))
                    .filter(portalShapePredicate)
            }
        }
    }
}