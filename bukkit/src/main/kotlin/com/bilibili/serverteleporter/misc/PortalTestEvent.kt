package com.bilibili.serverteleporter.misc

import org.bukkit.block.Block
import org.bukkit.block.BlockState
import org.bukkit.entity.Player
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.EquipmentSlot
import org.bukkit.inventory.ItemStack

class PortalTestEvent(
    placedBlock: Block,
    replacedBlockState: BlockState,
    placedAgainst: Block,
    itemInHand: ItemStack,
    val thePlayer: Player,
    canBuild: Boolean,
    hand: EquipmentSlot,
) : BlockPlaceEvent(placedBlock, replacedBlockState, placedAgainst, itemInHand, thePlayer, canBuild, hand) {
    override fun getPlayer(): Player {
        return object : Player by thePlayer {
            override fun sendMessage(message: String) {
                return
            }
        }
    }
}