package com.bilibili.serverteleporter.misc

import com.bilibili.serverteleporter.enums.Axis
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.EquipmentSlot
import org.bukkit.inventory.ItemStack
import java.util.*

object PortalGenerator {
    tailrec fun generatePortal(player: Player, creator: String, axis: Axis, id: Int): PortalData {
        val randomSafeLoc = randomSafeLoc(player)
        randomSafeLoc.y++
        val data = PortalData(creator, id, randomSafeLoc, axis, 3, 2)
        if (data.isSafeToCreate()) {
            return data.copy(bottomLeft = data.bottomLeft.clone().add(0.0, -1.0, 0.0))
        }
        return generatePortal(player, creator, axis, id)
    }

    private tailrec fun randomSafeLoc(player: Player): Location {
        val overWorld = Bukkit.getWorlds()[0]!!
        val center = overWorld.worldBorder.center
        val size = 10000
        val random = Random()
        val x = random.nextInt(size * 2) - size
        val z = random.nextInt(size * 2) - size

        val targetX = center.x + x
        val targetZ = center.z + z

        val highestBlockAt = overWorld.getHighestBlockAt(targetX.toInt(), targetZ.toInt())
//        if (highestBlockAt.location.add(0.0, -1.0, 0.0).block.isEmpty) return randomSafeLoc()
        if (highestBlockAt.location.add(0.0, -1.0, 0.0).block.isLiquid) return randomSafeLoc(player)
        val portalTestEvent = PortalTestEvent(highestBlockAt,
            highestBlockAt.state,
            highestBlockAt.getRelative(0, -1, 0),
            ItemStack(Material.AIR),
            player,
            true,
            EquipmentSlot.HAND)
        Bukkit.getPluginManager().callEvent(portalTestEvent)
        if (!portalTestEvent.isCancelled) {
            return highestBlockAt.location
        }
        return randomSafeLoc(player)
    }
}