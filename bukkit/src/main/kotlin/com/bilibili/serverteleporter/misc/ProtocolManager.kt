package com.bilibili.serverteleporter.misc

import com.bilibili.serverteleporter.api.events.PortalCreateFailureEvent
import kotlinx.coroutines.Job

object ProtocolManager {
    val failureEvents = HashMap<PortalCreateFailureEvent, Job>()
}