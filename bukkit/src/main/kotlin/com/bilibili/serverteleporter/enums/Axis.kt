@file:Suppress("unused", "unused", "unused", "unused")

package com.bilibili.serverteleporter.enums

import org.bukkit.util.Vector
import java.util.*

enum class Axis(val customName: String) {
    X("x") {
        override fun choose(x: Int, y: Int, z: Int): Int {
            return x
        }

        override fun choose(x: Double, y: Double, z: Double): Double {
            return x
        }
    },
    Y("y") {
        override fun choose(x: Int, y: Int, z: Int): Int {
            return y
        }

        override fun choose(x: Double, y: Double, z: Double): Double {
            return y
        }
    },
    Z("z") {
        override fun choose(x: Int, y: Int, z: Int): Int {
            return z
        }

        override fun choose(x: Double, y: Double, z: Double): Double {
            return z
        }
    };

    fun isVertical(): Boolean {
        return this == Y
    }

    fun isHorizontal(): Boolean {
        return this == X || this == Z
    }

    fun getPlane(): Plane {
        return when (this) {
            X, Z -> Plane.HORIZONTAL
            Y -> Plane.VERTICAL
            else -> throw Error("Someone's been tampering with the universe!")
        }
    }

    abstract fun choose(x: Int, y: Int, z: Int): Int

    abstract fun choose(x: Double, y: Double, z: Double): Double

    fun choose(loc: Vector): Double {
        return choose(loc.x, loc.y, loc.z)
    }

    fun toBukkit(): org.bukkit.Axis {
        return when (this) {
            X -> org.bukkit.Axis.X
            Y -> org.bukkit.Axis.Y
            Z -> org.bukkit.Axis.Z
        }
    }

    fun toVector(): Vector {
        return when (this) {
            X -> Vector(1.0, 0.0, 0.0)
            Y -> Vector(0.0, 1.0, 0.0)
            Z -> Vector(0.0, 0.0, 1.0)
            else -> throw Error("Someone's been tampering with the universe!")
        }
    }

    override fun toString(): String {
        return this.customName
    }

    companion object {
        private val VALUES = values()
        private val BY_NAME = VALUES.associateBy { it.customName }

        fun byName(name: String): Axis? {
            return BY_NAME[name.lowercase(Locale.ROOT)]
        }

        fun getRandom(random: Random): Axis {
            return VALUES[random.nextInt(VALUES.size)]
        }
    }
}