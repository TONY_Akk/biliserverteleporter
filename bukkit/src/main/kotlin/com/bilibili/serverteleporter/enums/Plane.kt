package com.bilibili.serverteleporter.enums

enum class Plane(private val axis: Array<Axis>) {
    HORIZONTAL(arrayOf(Axis.X, Axis.Z)), VERTICAL(arrayOf(Axis.Y));

//    fun getRandomAxis(random: Random): Axis {
//        return axis[random.nextInt(axis.size)]
//    }
}