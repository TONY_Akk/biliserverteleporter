package com.bilibili.serverteleporter

import com.bilibili.serverteleporter.api.events.PortalCreateFailureEvent
import com.bilibili.serverteleporter.command.CommandStper
import com.bilibili.serverteleporter.command.CommandTest
import com.bilibili.serverteleporter.listener.BKMessageListener
import com.bilibili.serverteleporter.listener.PortalListener
import com.bilibili.serverteleporter.misc.PortalManager
import com.bilibili.serverteleporter.misc.ProtocolManager
import com.bilibili.serverteleporter.network.CustomPacket
import com.bilibili.serverteleporter.network.PacketManager
import com.bilibili.serverteleporter.nms.NMSListener
import com.bilibili.serverteleporter.settings.BukkitSettings
import com.bilibili.serverteleporter.storage.IPortalStorage
import com.bilibili.serverteleporter.storage.YamlStorage
import com.bilibili.serverteleporter.utils.Logger
import com.bilibili.serverteleporter.utils.Sender
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.util.concurrent.atomic.AtomicReference

val logger = Logger.getUnformatted("ServerTeleporter", Sender { Bukkit.getConsoleSender().sendMessage(it) })
    .also { GlobalConst.logger = it }
lateinit var dataStorage: IPortalStorage/* = throw IllegalStateException("配置文件未加载")*/
val bukkitChannel = Channel<CustomPacket>()

class ServerTeleporterBukkit : JavaPlugin() {
    init {
        instance = this
    }

    override fun onEnable() {
        com.bilibili.serverteleporter.logger.info("§b正在加载 ServerTeleporter")

        com.bilibili.serverteleporter.logger.info("§b正在加载 配置文件")
        BukkitSettings
        dataStorage = YamlStorage
        com.bilibili.serverteleporter.logger.level = try {
            Logger.LoggerLevel.valueOf(BukkitSettings.settings.getString("loggerLevel", "INFO")!!)
        } catch (e: EnumConstantNotPresentException) {
            Logger.LoggerLevel.INFO.also { com.bilibili.serverteleporter.logger.info("§c加载日志等级失败, 使用默认日志等级") }
        }
        logger.fine("§配置文件 加载完成")
        com.bilibili.serverteleporter.logger.info("§b正在加载 传送门")
        PortalManager
        com.bilibili.serverteleporter.logger.fine("§b成功加载 ${PortalManager.getLoadedPortals().size} 个传送门")
        com.bilibili.serverteleporter.logger.info("§b正在加载 数据包模块...")
        PacketManager.availablePackets.forEach {
            Bukkit.getMessenger().registerIncomingPluginChannel(this, it.channel, BKMessageListener)
            Bukkit.getMessenger().registerOutgoingPluginChannel(this, it.channel)
        }
        // 加载数据包监听器.
        pluginScope.launch(Dispatchers.IO) {
            while (true) {
                if (Bukkit.getOnlinePlayers().isNotEmpty()) {
                    val packet = bukkitChannel.tryReceive().getOrNull() ?: continue
                    com.bilibili.serverteleporter.logger.verbose("正在尝试向Bungee端发送数据包: §b${packet.name}")
                    val event = PortalCreateFailureEvent(packet)
                    ProtocolManager.failureEvents[event] = pluginScope.launch {
                        delay(3000)
                    }
                    val packerSender = Bukkit.getOnlinePlayers().first()
                    val packetWrapper = AtomicReference<ByteArray>()
                    packet.write(packetWrapper)
                    packerSender.sendPluginMessage(instance,
                        packet.channel,
                        packetWrapper.get() ?: throw IllegalStateException("写入数据包出现问题, 请向作者反馈."))
                }
            }
        }
        com.bilibili.serverteleporter.logger.fine("§a数据包模块 加载完成")

        com.bilibili.serverteleporter.logger.info("§b正在加载 命令模块...")
        Bukkit.getPluginCommand("tstper")!!.setExecutor(CommandTest)
        Bukkit.getPluginCommand("serverteleporter")!!.setExecutor(CommandStper)
        com.bilibili.serverteleporter.logger.fine("§a命令模块 加载完成")
        Bukkit.getPluginManager().registerEvents(PortalListener, this)
        Bukkit.getPluginManager().registerEvents(NMSListener, this)
    }

    override fun onDisable() {

    }

    companion object {
        lateinit var instance: ServerTeleporterBukkit
    }
}

fun main() {
    val scope = CoroutineScope(Job() + CoroutineExceptionHandler { ctx, except ->
        println(except.message)
    })
    runBlocking {
        scope.launch {
            println("FW")
            throw IllegalArgumentException("PC NM\$L")
        }.join()

        scope.launch {
            val pcName = "PC_Dawn"
            println("PC - $pcName")
            println("NMHL")
        }.join()
    }
}