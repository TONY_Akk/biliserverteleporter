package com.bilibili.serverteleporter.command

import com.bilibili.serverteleporter.misc.PortalManager
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionDefault
import kotlin.math.roundToInt

object CommandStper : CommandExecutor, TabCompleter {
    init {
        Bukkit.getPluginManager().apply {
            addPermission(Permission("stper.list").apply { default = PermissionDefault.TRUE })
            addPermission(Permission("stper.near").apply { default = PermissionDefault.TRUE })
            addPermission(Permission("stper.remove").apply { default = PermissionDefault.OP })
        }
    }

    override fun onCommand(player: CommandSender, command: Command, string: String, args: Array<String>): Boolean {
        if (player !is Player) {
            return true.also { player.sendMessage("§c| §7该命令只能由玩家执行") }
        }

        if (args.isEmpty()) {
            val help = StringBuffer("""
                    
            §bServerTeleporter §8 v1.0.2 - by TONY_All
            
              §7命令:
            """.trimIndent())
            help.appendLine()
            if (player.hasPermission("stper.list")) help.appendLine(" §3| §bstper list §7- 显示所有可用传送门")
            if (player.hasPermission("stper.near")) help.appendLine(" §3| §bstper near §7- 显示离你最近的传送门")
            if (player.hasPermission("stper.remove")) help.appendLine(" §6| §bstper remove §8<id> §7- 删除指定传送门")
            player.sendMessage(*help.lines().toTypedArray())
            return true
        }
        when (args[0]) {
            "list" -> {
                player.sendMessage("")
                player.sendMessage("§7 当前可用传送门: ")
                player.sendMessage("")
                val sortedPortal = PortalManager.getLoadedPortals().sortedBy { it.id }
                if (sortedPortal.size > 7) {
                    repeat(7) {
                        val portalData = sortedPortal[it]
                        val loc = portalData.bottomLeft
                        player.sendMessage("§a| §7${portalData.id}, 坐标: (§a${loc.blockX}, ${loc.blockY}, ${loc.blockZ}§7)")
                    }
                    player.sendMessage("§e| §7..等${sortedPortal.size - 7}个")
                } else {
                    sortedPortal.forEach {
                        val loc = it.bottomLeft
                        player.sendMessage("§a| §7${it.id}, 坐标: (§a${loc.blockX}, ${loc.blockY}, ${loc.blockZ}§7)")
                    }
                }
                return true
            }
            "near" -> {
                if (!player.hasPermission("stper.near")) return true.also { player.sendMessage("§c| §7你没有权限执行该指令.") }
                val portalData = PortalManager.getLoadedPortals()
                    .filter { it.bottomLeft.distance(player.location) <= 500 }
                    .sortedWith { pd1, pd2 ->
                        return@sortedWith (pd1.bottomLeft.distance(player.location) - pd2.bottomLeft.distance(player.location)).roundToInt()
                    }
                    .firstOrNull() ?: return true.also { player.sendMessage("§6| §7你附近没有可用的传送门") }
                player.location.toVector()
                val bottomLeft = portalData.bottomLeft.toVector()
                player.sendMessage("§a| §7离你最近的传送门位于 (§a${bottomLeft.blockX}, ${bottomLeft.blockY}, ${bottomLeft.blockZ}§7)")
                return true
            }
            "remove" -> {
                if (!player.hasPermission("stper.remove")) return true.also { player.sendMessage("§c| §7你没有权限执行该指令.") }
                if (args.size != 2) return true.also {
                    player.sendMessage("§c| §7你未输入ID.",
                        "§6| §7正确用法: §b/stper remove §8<id>")
                }
                val id = args[1].toIntOrNull() ?: return true.also { player.sendMessage("§c| §7你输入的ID非法.") }
                val portalData = PortalManager.getPortalByID(id)
                    ?: return true.also { player.sendMessage("§c| §7ID为§c$id§7的传送门不存在.") }
                PortalManager.removePortal(portalData)
                portalData.getPortalBlocks().forEach {
                    val loc = it.toLocation(portalData.bottomLeft.world!!)
                    loc.block.type = Material.AIR
                }
                player.sendMessage("§a| §7成功删除传送门, 请保证对应服务器有人以使插件能够删除对应传送门")
                return true
            }
        }
        return true
    }

    override fun onTabComplete(p0: CommandSender, p1: Command, p2: String, args: Array<String>): MutableList<String> {
        if (args.size == 1) {
            return listOf("list", "near", "remove").filterArguments(args[0])
        }
        when (args[0]) {
            "remove" -> listOf(PortalManager.getLoadedPortals().map { it.id.toString() }.filterArguments(args[1]))
        }
        return emptyList<String>().toMutableList()
    }

    private fun List<String>.filterArguments(current: String): MutableList<String> {
        return filter { it.startsWith(current) }
            .filterIndexed { index, _ -> index <= 10 }
            .toMutableList()
    }
}