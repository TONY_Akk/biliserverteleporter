package com.bilibili.serverteleporter.command

import com.bilibili.serverteleporter.bukkitChannel
import com.bilibili.serverteleporter.enums.Axis
import com.bilibili.serverteleporter.misc.PortalData
import com.bilibili.serverteleporter.misc.PortalGenerator
import com.bilibili.serverteleporter.network.packets.PortalCreatePacket
import com.bilibili.serverteleporter.nms.SignSender.askContinue
import com.bilibili.serverteleporter.nms.SignSender.sendSign
import com.bilibili.serverteleporter.pluginScope
import kotlinx.coroutines.launch
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object CommandTest : CommandExecutor, TabCompleter {
    override fun onCommand(player: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (player !is Player) return true
        if (args.isEmpty()) return true.also { player.sendMessage("Nothing done.") }
        when (args[0]) {
            "createPortal" -> {
                val id = args.getOrNull(1)?.toInt() ?: return true.also { player.sendMessage("§7缺少参数: §bid") }
                val targetBlockExact =
                    player.getTargetBlockExact(20) ?: return true.also { player.sendMessage("§c无法找到你所指向的方块.") }
                targetBlockExact.type = Material.AIR
                PortalData(player.name, id, targetBlockExact.location, Axis.X, 3, 2).apply {
                    createFrame()
                    createPortal()
                }
                player.sendMessage("§a成功生成传送门")
            }
            "sendPacket" -> {
                val id = args.getOrNull(1)?.toInt() ?: return true.also { player.sendMessage("§c缺少参数: §bid") }
                val packet = PortalCreatePacket(id, player.name, Axis.X.customName)
                pluginScope.launch {
                    bukkitChannel.send(packet)
                    player.sendMessage("数据包发送成功")
                }
            }
            "generatePortal" -> {
                val id = args.getOrNull(1)?.toInt() ?: return true.also { player.sendMessage("§7缺少参数: §bid") }
                val generatePortal = PortalGenerator.generatePortal(player, player.name, Axis.X, id).apply {
                    createFrame()
                    createPortal()
                }
                val bL = generatePortal.bottomLeft
                player.sendMessage("§a成功生成传送门, §b位置: ${bL.blockX}, ${bL.blockY}, ${bL.blockZ}")
            }
            "testSign" -> {
                val defaultContent = listOf("", "§b| §8上方输入ID")
                pluginScope.launch {
                    val result = ArrayList<List<String>>()
                    player.sendMessage("§7| §b打开告示牌UI")
                    result.add(player.sendSign(defaultContent))
                    while (player.askContinue()) {
                        player.sendMessage("§7| §b打开告示牌UI")
                        result.add(player.sendSign(defaultContent))
                    }
                    player.sendMessage("§a| §f结果: §7${result.map { it[0] }}")
                }
            }
            "testSafe" -> {
                val id = args.getOrNull(1)?.toInt() ?: return true.also { player.sendMessage("§7缺少参数: §bid") }
                val targetBlockExact =
                    player.getTargetBlockExact(20) ?: return true.also { player.sendMessage("§c无法找到你所指向的方块.") }
                targetBlockExact.type = Material.AIR
                val safe = PortalData(player.name, id, targetBlockExact.location, Axis.X, 3, 2).isSafeToCreate()
                player.sendMessage("§6| §7$safe")
            }
            else -> {
                player.sendMessage("Nothing done.")
            }
        }
        return true
    }

    override fun onTabComplete(
        player: CommandSender,
        command: Command,
        alias: String,
        args: Array<String>,
    ): MutableList<String> {
        if (player !is Player) return emptyList<String>().toMutableList()
        if (args.size == 1) {
            return listOf("createPortal", "sendPacket", "generatePortal", "testSign")
                .filter { it.startsWith(args.getOrNull(0) ?: "") }
                .toMutableList()
        }
        return emptyList<String>().toMutableList()
    }

}