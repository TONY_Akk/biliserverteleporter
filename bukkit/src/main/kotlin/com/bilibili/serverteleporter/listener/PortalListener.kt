package com.bilibili.serverteleporter.listener

import com.bilibili.serverteleporter.api.events.PacketEvent
import com.bilibili.serverteleporter.api.events.PortalPostCreateEvent
import com.bilibili.serverteleporter.api.events.PortalPreCreateEvent
import com.bilibili.serverteleporter.bukkitChannel
import com.bilibili.serverteleporter.enums.Axis
import com.bilibili.serverteleporter.logger
import com.bilibili.serverteleporter.misc.PortalCalculator
import com.bilibili.serverteleporter.misc.PortalData
import com.bilibili.serverteleporter.misc.PortalGenerator
import com.bilibili.serverteleporter.misc.PortalManager
import com.bilibili.serverteleporter.network.packets.PortalDestroyPacket
import com.bilibili.serverteleporter.network.packets.PortalTeleportPacket
import com.bilibili.serverteleporter.nms.SignSender.sendSign
import com.bilibili.serverteleporter.pluginScope
import com.bilibili.serverteleporter.utils.bukkitSyncContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.*
import org.bukkit.event.entity.EntityExplodeEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerPortalEvent
import org.bukkit.util.Vector

object PortalListener : Listener {
    private val cache = ArrayList<Vector>()

    @EventHandler
    fun onIgnite(event: PlayerInteractEvent) {
        val player = event.player
        val block = event.clickedBlock ?: return
        if (block.world != Bukkit.getWorlds()[0]) return

        if (!block.type.name.contains("LOG")) return
        if (!event.hasItem()) return
        if (event.item!!.type != Material.FLINT_AND_STEEL) return

        if (event.action != Action.RIGHT_CLICK_BLOCK) return

        var portal: PortalCalculator? = null
        PortalCalculator.findEmptyPortalShape(block.location.add(event.blockFace.direction), Axis.X).ifPresent {
            portal = it
        }
        val portalShadow = portal ?: return
        if (!portalShadow.isValid()) return
        val portalPreCreateEvent = PortalPreCreateEvent(player, portalShadow)
        Bukkit.getPluginManager().callEvent(portalPreCreateEvent)
        if (portalPreCreateEvent.isCancelled) {
            return
        }

        cache.add(block.location.add(event.blockFace.direction).toVector())

        pluginScope.launch {
            player.sendMessage("§b| §7请在告示牌第一行设置ID")
            val sign = player.sendSign(listOf("§e", "§8↑", "§8|", "§b上方输入ID §8(数字)"))
            val id = sign[0].replace("§e", "").toIntOrNull()
                ?: return@launch player.sendMessage("§c| §7传送门创建失败: id (§c${
                    sign[0].replace("§e",
                        "")
                }§7) 不合法, 应为数字.")
            if (PortalManager.getPortalByID(id) != null) {
                player.sendMessage("§c| §7ID为 §c$id §7的传送门已存在, 无法重复建立.")
                return@launch
            }
            // 同步任务
            withContext(bukkitSyncContext) {
                portalShadow.createPortalBlocks()
                val portalData = PortalData(player.name,
                    id,
                    portalShadow.bottomLeft,
                    portalShadow.axis,
                    portalShadow.height,
                    portalShadow.width)
                val portalPostCreateEvent = PortalPostCreateEvent(player, portalData)
                Bukkit.getPluginManager().callEvent(portalPostCreateEvent)
                PortalManager.addPortal(portalData)
                player.sendMessage("§a| §7传送门创建成功, ID为§a $id")
            }
        }
    }

    @EventHandler
    fun onIgnite(e: BlockIgniteEvent) {
        if (cache.contains(e.block.location.toVector())) {
            cache.remove(e.block.location.toVector())
            pluginScope.launch {
                delay(200)
                withContext(bukkitSyncContext) {
                    e.block.type = Material.AIR
                }
            }
        }
        PortalManager.getLoadedPortals().forEach {
            val necessary = it.getNecessaryBlocks()
            BlockFace.values().forEach face@{ face ->
                if (!necessary.contains(e.block.location.add(face.direction).toVector())) {
                    return@forEach
                }
                e.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onPacket(e: PacketEvent) {
        when (val packet = e.packet) {
            is PortalTeleportPacket -> {
                logger.verbose("§b收到传送门传送数据包, 正在传送玩家.")
                pluginScope.launch {
                    delay(500)
                    withContext(bukkitSyncContext) {
                        val player = Bukkit.getPlayer(packet.player)
                            ?: return@withContext logger.error("玩家 ${packet.player} 不存在")
                        val portalData = PortalManager.getPortalByID(packet.id) ?: PortalGenerator
                            .generatePortal(player, packet.creator, Axis.byName(packet.axis) ?: Axis.X, packet.id)
                            .also {
                                logger.verbose("""
                            §e传送门信息:
                            §f建立者: ${packet.creator}
                            ID: ${packet.id}
                            方向: ${packet.axis}
                        """.trimIndent())
                                it.getNecessaryBlocks().forEach { vec ->
                                    vec.toLocation(player.world).block.type = Material.AIR
                                }
                                it.createFrame()
                                it.createPortal()
                                PortalManager.addPortal(it)
                            }
                        player.portalCooldown = 60
                        player.teleport(portalData.safeLoc)
                    }
                }
                return
            }
            is PortalDestroyPacket -> {
                val portalData =
                    PortalManager.getPortalByID(packet.id) ?: return logger.error("§c${packet.id} 的传送门不存在, 无法破坏.")
                pluginScope.launch(bukkitSyncContext) {
                    breakPortal(portalData = portalData, sendPacket = false)
                    logger.verbose("移除传送门 ${packet.id}")
                    PortalManager.removePortal(portalData)
                }
            }
        }
    }

    @EventHandler
    fun onPortalTeleport(e: PlayerPortalEvent) {
        PortalManager.getLoadedPortals().forEach {
            it.getPortalBlocks().firstOrNull { vec -> vec.distance(e.from.toVector()) <= 2 } ?: return@forEach
            e.isCancelled = true
            pluginScope.launch {
                bukkitChannel.send(PortalTeleportPacket(id = it.id,
                    creator = it.creator,
                    axis = it.axis.customName,
                    player = e.player.uniqueId))
            }
            return
        }
    }

    @EventHandler
    fun onPlaceWater(e: PlayerInteractEvent) {
        if (e.action != Action.RIGHT_CLICK_BLOCK) return
        if (!e.hasItem()) return
        if (!e.item!!.type.name.contains("BUCKET")) return
        if (e.item!!.type == Material.MILK_BUCKET || e.item!!.type == Material.POWDER_SNOW_BUCKET) return
        val placeWater = e.clickedBlock!!.location.clone().add(e.blockFace.direction)
        PortalManager.getLoadedPortals().forEach {
            if (!it.getNecessaryBlocks().contains(placeWater.toVector())) {
                return@forEach
            }
            if (e.player.hasPermission("tstper.bypass")) {
                breakPortal(e.player, it)
                return
            }
            if (e.player.name != it.creator) {
                e.player.sendMessage("§c| §7该传送门不是你的, 你没有权限破坏.")
                e.isCancelled = true
                return
            }
            breakPortal(e.player, it)
        }
    }

    @EventHandler
    fun onPlace(e: BlockDispenseEvent) {
        if (e.item.type != Material.WATER_BUCKET) return
        val data = e.block.state.blockData as org.bukkit.block.data.type.Dispenser
        val target = e.block.location.add(data.facing.direction)
        PortalManager.getLoadedPortals().forEach {
            if (!it.getNecessaryBlocks().contains(target.toVector())) {
                return@forEach
            }
            e.isCancelled = true
            return
        }
    }

    @EventHandler
    fun onFire(e: BlockBurnEvent) {
        PortalManager.getLoadedPortals().forEach {
            if (!it.getNecessaryBlocks().contains(e.block.location.toVector())) {
                return@forEach
            }
            e.isCancelled = true
        }
    }

    @EventHandler
    fun onTNTExplode(e: EntityExplodeEvent) {
        val remove = ArrayList<Block>()
        PortalManager.getLoadedPortals().forEach {
            e.blockList().forEach inner@{ block ->
                if (!it.getNecessaryBlocks().contains(block.location.toVector())) {
                    return@inner
                }
                remove.add(block)
            }
        }
        e.blockList().removeAll(remove)
    }

    @EventHandler
    fun onPortalBreakByBreak(e: BlockBreakEvent) {
        if (!e.block.type.name.contains("LOG") && e.block.type != Material.NETHER_PORTAL) return
        PortalManager.getLoadedPortals().forEach {
            if (!it.getNecessaryBlocks().contains(e.block.location.toVector())) {
                return@forEach
            }
            if (e.player.hasPermission("tstper.bypass")) {
                breakPortal(e.player, it)
                return
            }
            if (e.player.name != it.creator) {
                e.player.sendMessage("§c| §7该传送门不是你的, 你没有权限破坏.")
                e.isCancelled = true
                return
            }
            breakPortal(e.player, it)
        }
    }

    @EventHandler
    fun onPortalCreate(e: PortalPreCreateEvent) {
        var portalCount = 2
        e.player.recalculatePermissions()
        e.player.effectivePermissions.forEach {
            logger.verbose(it.permission)
            if (it.permission.startsWith("stper.portal_count.")) {
                val count = it.permission.replace("stper.portal_count.", "").toIntOrNull() ?: return@forEach
                if (count > portalCount) {
                    portalCount = count
                }
            }
        }

        val currentPortalCount = PortalManager.getLoadedPortals().filter { it.creator == e.player.name }.size
        if (currentPortalCount >= portalCount) {
            e.isCancelled = true
            e.player.sendMessage("§6| §f你的传送门数量已达上限, 无法创建更多.")
            return
        }
    }

    private fun breakPortal(player: Player? = null, portalData: PortalData, sendPacket: Boolean = true) {
        player?.sendMessage("§e| §7传送门 §e${portalData.id} §7已被破坏")
        if (sendPacket) {
            pluginScope.launch {
                bukkitChannel.send(PortalDestroyPacket(portalData.id, player?.name ?: ""))
            }
        }
        PortalManager.removePortal(portalData)
        portalData.getPortalBlocks().forEach {
            val loc = it.toLocation(portalData.bottomLeft.world!!)
            loc.block.type = Material.AIR
        }
    }
}