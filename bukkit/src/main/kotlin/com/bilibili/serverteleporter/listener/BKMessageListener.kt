package com.bilibili.serverteleporter.listener

import com.bilibili.serverteleporter.api.events.PacketEvent
import com.bilibili.serverteleporter.logger
import com.bilibili.serverteleporter.network.PacketManager
import com.bilibili.serverteleporter.pluginScope
import com.bilibili.serverteleporter.utils.bukkitAsyncContext
import kotlinx.coroutines.launch
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.plugin.messaging.PluginMessageListener
import java.util.concurrent.atomic.AtomicReference

object BKMessageListener : PluginMessageListener {
    override fun onPluginMessageReceived(channel: String, player: Player, message: ByteArray) {
        logger.verbose("从 $channel 接收到数据包")
        val packet = PacketManager.getPacketByChannel(channel) ?: return
        packet.read(AtomicReference(message))
        logger.verbose("接收到数据包: $packet")
        pluginScope.launch(bukkitAsyncContext) {
            Bukkit.getPluginManager().callEvent(PacketEvent(packet))
        }
    }
}