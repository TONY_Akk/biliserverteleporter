repositories {
    maven("http://ptms.ink:8081/repository/maven-public/") {
        isAllowInsecureProtocol = true
    }
}

dependencies {
//    compileOnly("org.spigotmc:spigot-api:1.17.1-R0.1-SNAPSHOT")
    compileOnly("ink.ptms.core.universal:v11701:11701")
//    implementation("ink.ptms.core.universal:v11701:11701")
    compileOnly("org.spigotmc:plugin-annotations:1.2.3-SNAPSHOT") {
        exclude(group = "org.bukkit")
    }
    annotationProcessor("org.spigotmc:plugin-annotations:1.2.3-SNAPSHOT")
    implementation(project(":common"))
}
