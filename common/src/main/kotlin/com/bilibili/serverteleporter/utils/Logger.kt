@file:Suppress("unused")

package com.bilibili.serverteleporter.utils

class Logger(
    private val pattern: String,
    private val name: String,
    private val sender: Sender,
    @Suppress("MemberVisibilityCanBePrivate") var level: LoggerLevel,
) {
    companion object {
        @JvmStatic
        fun getUnformatted(name: String, sender: Sender): Logger {
            return Logger("§8[§3§l{0}§8][§r{1}§8] §f{2}", name, sender, LoggerLevel.VERBOSE)
        }
    }

    fun verbose(msg: String) {
        if (level.ordinal <= LoggerLevel.VERBOSE.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§fVERBOSE",
                msg.replace("&", "§"))
        }
    }

    fun finest(msg: String) {
        if (level.ordinal <= LoggerLevel.FINEST.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§eFINEST",
                msg.replace("&", "§")
            )
        }
    }

    fun fine(msg: String) {
        if (level.ordinal <= LoggerLevel.FINE.ordinal) {

            sender.sendMessage(
                pattern,
                name,
                "§aFINE",
                msg.replace("&", "§"))
        }
    }

    fun info(msg: String) {
        if (level.ordinal <= LoggerLevel.INFO.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§bINFO",
                msg.replace("&", "§"))
        }
    }

    fun warn(msg: String) {
        if (level.ordinal <= LoggerLevel.WARN.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§6WARN",
                "§6" + msg.replace("&", "§"))
        }
    }

    fun error(msg: String) {
        if (level.ordinal <= LoggerLevel.ERROR.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§cERROR",
                "§c" + msg.replace("&", "§"))
        }
    }

    fun fatal(msg: String) {
        if (level.ordinal <= LoggerLevel.FATAL.ordinal) {
            sender.sendMessage(
                pattern,
                name,
                "§4FATAL",
                "§4" + msg.replace("&", "§"))
        }
    }

    enum class LoggerLevel {
        VERBOSE,
        FINEST,
        FINE,
        INFO,
        WARN,
        ERROR,
        FATAL
    }
}

