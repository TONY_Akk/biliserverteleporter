@file:Suppress("unused")

package com.bilibili.serverteleporter.utils

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream
import kotlin.math.max

/**
 * 文本工具
 *
 * @author sky
 * @since 2021-5-23 2:43p.m.
 */
object Strings {
    /**
     * 重复一段文本特定次数
     *
     * @param text  文本
     * @param count 次数
     * @return String
     */
    fun copy(text: String?, count: Int): String {
        return IntStream.range(0, count).mapToObj { text }.collect(Collectors.joining())
    }

    fun nonBlack(`var`: String?): Boolean {
        return !isBlank(`var`)
    }

    private fun isBlank(`var`: String?): Boolean {
        return `var` == null || `var`.trim { it <= ' ' }.isEmpty()
    }

    fun nonEmpty(`var`: CharSequence?): Boolean {
        return !isEmpty(`var`)
    }

    private fun isEmpty(`var`: CharSequence?): Boolean {
        return `var` == null || `var`.isEmpty()
    }

    @JvmOverloads
    fun hashKeyForDisk(key: String, type: String? = "MD5"): String {
        return try {
            val mDigest = MessageDigest.getInstance(type)
            mDigest.update(key.toByteArray())
            bytesToHexString(mDigest.digest())
        } catch (e: NoSuchAlgorithmException) {
            key.hashCode().toString()
        }
    }

    fun hashKeyForDisk(key: ByteArray?, type: String?): String {
        return try {
            val mDigest = MessageDigest.getInstance(type)
            mDigest.update(key)
            bytesToHexString(mDigest.digest())
        } catch (e: NoSuchAlgorithmException) {
            Arrays.hashCode(key).toString()
        }
    }

    /**
     * 获取两段文本的相似度（0.0~1.0)
     *
     * @param strA 文本
     * @param strB 文本
     * @return double
     */
    fun similarDegree(strA: String, strB: String): Double {
        val newStrA = removeSign(max(strA, strB))
        val newStrB = removeSign(min(strA, strB))
        return try {
            val temp = max(newStrA.length, newStrB.length)
            val temp2 = longestCommonSubstring(newStrA, newStrB).length
            temp2 * 1.0 / temp
        } catch (ignored: Exception) {
            0.0
        }
    }

    private fun bytesToHexString(bytes: ByteArray): String {
        val sb = StringBuilder()
        for (aByte in bytes) {
            val hex = Integer.toHexString(0xFF and aByte.toInt())
            if (hex.length == 1) {
                sb.append('0')
            }
            sb.append(hex)
        }
        return sb.toString()
    }

    private fun max(strA: String, strB: String): String {
        return if (strA.length >= strB.length) strA else strB
    }

    private fun min(strA: String, strB: String): String {
        return if (strA.length < strB.length) strA else strB
    }

    private fun removeSign(str: String): String {
        val builder = StringBuilder()
        for (item in str.toCharArray()) {
            if (charReg(item)) {
                builder.append(item)
            }
        }
        return builder.toString()
    }

    private fun charReg(charValue: Char): Boolean {
        return charValue.code in 0x4E00..0X9FA5 || charValue in 'a'..'z' || charValue in 'A'..'Z' || charValue in '0'..'9'
    }

    private fun longestCommonSubstring(strA: String, strB: String): String {
        val charsStrA = strA.toCharArray()
        val charsStrB = strB.toCharArray()
        var m = charsStrA.size
        var n = charsStrB.size
        val matrix = Array(m + 1) { IntArray(n + 1) }
        for (i in 1..m) {
            for (j in 1..n) {
                if (charsStrA[i - 1] == charsStrB[j - 1]) {
                    matrix[i][j] = matrix[i - 1][j - 1] + 1
                } else {
                    matrix[i][j] = matrix[i][j - 1].coerceAtLeast(matrix[i - 1][j])
                }
            }
        }
        val result = CharArray(matrix[m][n])
        var currentIndex = result.size - 1
        while (matrix[m][n] != 0) {
            when {
                matrix[n].contentEquals(matrix[n - 1]) -> {
                    n--
                }
                matrix[m][n] == matrix[m - 1][n] -> {
                    m--
                }
                else -> {
                    result[currentIndex] = charsStrA[m - 1]
                    currentIndex--
                    n--
                    m--
                }
            }
        }
        return String(result)
    }
}

/**
 * 优化过的 String#replace，比默认快了大概 5 倍
 *
 * @param template 模板替换文件
 * @param args     替换的参数
 * @return 替换好的字符串
 */
fun replaceWithOrder(template: String, vararg args: Any): String {
    if (args.isEmpty() || template.isEmpty()) {
        return template
    }
    val arr = template.toCharArray()
    val stringBuilder = StringBuilder(template.length)
    var i = 0
    while (i < arr.size) {
        val mark = i
        if (arr[i] == '{') {
            var num = 0
            while (i + 1 < arr.size && Character.isDigit(arr[i + 1])) {
                i++
                num *= 10
                num += arr[i] - '0'
            }
            if (i != mark && i + 1 < arr.size && arr[i + 1] == '}') {
                i++
                stringBuilder.append(args[num])
            } else {
                i = mark
            }
        }
        if (mark == i) {
            stringBuilder.append(arr[i])
        }
        i++
    }
    return stringBuilder.toString()
}