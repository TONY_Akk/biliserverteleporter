package com.bilibili.serverteleporter.utils

fun Sender.sendMessage(string: String, vararg replacements: String) {
    sendMessage(replaceWithOrder(string, *replacements))
}