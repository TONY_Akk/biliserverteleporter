package com.bilibili.serverteleporter.utils

class Sender(private val sendMessageFunction: (String) -> Unit) {
    fun sendMessage(string: String) = sendMessageFunction(string)
}