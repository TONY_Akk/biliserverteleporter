package com.bilibili.serverteleporter

import com.bilibili.serverteleporter.utils.Logger
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

object GlobalConst {
    lateinit var logger: Logger
}

val pluginScope = CoroutineScope(SupervisorJob() + CoroutineExceptionHandler { _, except ->
    GlobalConst.logger.error("§c执行异步操作时出现异常 ${except.message}")
    GlobalConst.logger.error("栈追踪: ")
    except.stackTrace.forEach {
        GlobalConst.logger.error("位于 $it")
    }
})