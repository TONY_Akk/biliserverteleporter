package com.bilibili.serverteleporter.network

import java.util.concurrent.atomic.AtomicReference

interface CustomPacket : Cloneable {
    val name: String

    val channel: String
        get() = "teleporter:$name"

    fun read(data: AtomicReference<ByteArray>)

    fun write(data: AtomicReference<ByteArray>)
}