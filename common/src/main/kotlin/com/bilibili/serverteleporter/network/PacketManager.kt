package com.bilibili.serverteleporter.network

import com.bilibili.serverteleporter.network.packets.PortalCreatePacket
import com.bilibili.serverteleporter.network.packets.PortalDestroyPacket
import com.bilibili.serverteleporter.network.packets.PortalTeleportPacket

object PacketManager {
    val availablePackets = listOf(
        PortalCreatePacket(),
        PortalTeleportPacket(),
        PortalDestroyPacket()
    )
    private val channelCache = availablePackets.associateBy { it.channel }

    fun getPacketByChannel(channel: String): CustomPacket? {
        return channelCache[channel]?.javaClass?.newInstance()
    }
}