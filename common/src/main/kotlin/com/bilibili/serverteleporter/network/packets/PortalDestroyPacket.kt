package com.bilibili.serverteleporter.network.packets

import com.bilibili.serverteleporter.network.CustomPacket
import com.google.common.io.ByteStreams
import java.util.concurrent.atomic.AtomicReference

@Suppress("UnstableApiUsage")
data class PortalDestroyPacket(var id: Int = 0, var destroyer: String = "") : CustomPacket {
    override val name: String
        get() = "destroy"

    override fun read(data: AtomicReference<ByteArray>) {
        val dataInput = ByteStreams.newDataInput(data.get())
        id = dataInput.readInt()
        destroyer = dataInput.readUTF()
    }

    override fun write(data: AtomicReference<ByteArray>) {
        val dataOutput = ByteStreams.newDataOutput()
        dataOutput.writeInt(id)
        dataOutput.writeUTF(destroyer)
        data.set(dataOutput.toByteArray())
    }
}