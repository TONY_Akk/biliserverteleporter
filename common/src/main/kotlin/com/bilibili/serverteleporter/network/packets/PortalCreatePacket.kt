package com.bilibili.serverteleporter.network.packets

import com.bilibili.serverteleporter.network.CustomPacket
import com.google.common.io.ByteStreams
import java.util.concurrent.atomic.AtomicReference

@Suppress("UnstableApiUsage")
@Deprecated("Useless")
data class PortalCreatePacket(var id: Int = 0, var creator: String = "", var axis: String = "") : CustomPacket {
    override val name: String = "create"

    override fun read(data: AtomicReference<ByteArray>) {
        val dataInput = ByteStreams.newDataInput(data.get())
        id = dataInput.readInt()
        creator = dataInput.readUTF()
        axis = dataInput.readUTF()
    }

    override fun write(data: AtomicReference<ByteArray>) {
        val dataOutput = ByteStreams.newDataOutput()
        dataOutput.writeInt(id)
        dataOutput.writeUTF(creator)
        dataOutput.writeUTF(axis)
        data.set(dataOutput.toByteArray())
    }
}