package com.bilibili.serverteleporter.network.packets

import com.bilibili.serverteleporter.network.CustomPacket
import com.google.common.io.ByteStreams
import java.util.*
import java.util.concurrent.atomic.AtomicReference

@Suppress("UnstableApiUsage")
data class PortalTeleportPacket constructor(
    var id: Int = 0,
    var creator: String = "",
    var axis: String = "",
    var player: UUID = UUID.randomUUID(),
) : CustomPacket {
    override val name: String
        get() = "teleport"

    override fun read(data: AtomicReference<ByteArray>) {
        val dataInput = ByteStreams.newDataInput(data.get())
        id = dataInput.readInt()
        creator = dataInput.readUTF()
        axis = dataInput.readUTF()
        player = UUID.fromString(dataInput.readUTF())
    }

    override fun write(data: AtomicReference<ByteArray>) {
        val dataOutput = ByteStreams.newDataOutput()
        dataOutput.writeInt(id)
        dataOutput.writeUTF(creator)
        dataOutput.writeUTF(axis)
        dataOutput.writeUTF(player.toString())
        data.set(dataOutput.toByteArray())
    }
}