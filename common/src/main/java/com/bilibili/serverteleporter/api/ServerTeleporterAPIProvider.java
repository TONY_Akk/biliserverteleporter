package com.bilibili.serverteleporter.api;

import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

public class ServerTeleporterAPIProvider {
    private static ServerTeleporterAPI api = null;

    public static @NotNull ServerTeleporterAPI get() {
        if (api == null) {
            throw new IllegalStateException("ServerTeleporterAPI hasn't not loaded");
        }
        return api;
    }

    @ApiStatus.Internal
    public static void register(ServerTeleporterAPI api) {
        ServerTeleporterAPIProvider.api = api;
    }
}
