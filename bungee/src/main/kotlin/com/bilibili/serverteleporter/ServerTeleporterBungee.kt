package com.bilibili.serverteleporter

import com.bilibili.serverteleporter.listener.BCMessageListener
import com.bilibili.serverteleporter.network.CustomPacket
import com.bilibili.serverteleporter.network.PacketManager
import com.bilibili.serverteleporter.settings.BungeeSettings
import com.bilibili.serverteleporter.utils.Logger
import com.bilibili.serverteleporter.utils.Sender
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.api.plugin.Plugin
import java.util.concurrent.atomic.AtomicReference

val logger = Logger.getUnformatted("ServerTeleporter",
    Sender { ProxyServer.getInstance().console.sendMessage(TextComponent(it)) }).also { GlobalConst.logger = it }

val bungeeChannel = Channel<Pair<ServerInfo, CustomPacket>>()

@Suppress("UnstableApiUsage")
class ServerTeleporterBungee : Plugin() {
    init {
        instance = this
    }

    @ExperimentalCoroutinesApi
    override fun onEnable() {
        com.bilibili.serverteleporter.logger.info("§b正在加载 ServerTeleporter...")

        com.bilibili.serverteleporter.logger.info("§b正在加载 配置文件...")
        BungeeSettings.settings.getString("")
        com.bilibili.serverteleporter.logger.level = try {
            Logger.LoggerLevel.valueOf(BungeeSettings.settings.getString("loggerLevel", "INFO")!!)
        } catch (e: EnumConstantNotPresentException) {
            Logger.LoggerLevel.INFO.also { com.bilibili.serverteleporter.logger.info("§c加载日志等级失败, 使用默认日志等级") }
        }
        com.bilibili.serverteleporter.logger.fine("§a配置文件 加载成功")

        com.bilibili.serverteleporter.logger.info("§b正在加载 数据包模块...")
        PacketManager.availablePackets.forEach {
            proxy.registerChannel(it.channel)
        }
        proxy.pluginManager.registerListener(this, BCMessageListener)
        // launch Packet Sender
        pluginScope.launch {
            while (!bungeeChannel.isClosedForReceive) {
                val received = bungeeChannel.receive()
                pluginScope.launch(Dispatchers.Default) {
                    val packetWrapper = AtomicReference<ByteArray>()
                    received.second.write(packetWrapper)
                    received.first.sendData(received.second.channel, packetWrapper.get(), true)
                    com.bilibili.serverteleporter.logger.verbose("向 ${received.first.name} 发送数据包: ${received.second}")
                }
            }
        }
        com.bilibili.serverteleporter.logger.fine("§a数据包模块 加载成功")
    }

    companion object {
        lateinit var instance: ServerTeleporterBungee
    }
}
