package com.bilibili.serverteleporter.api.events

import com.bilibili.serverteleporter.network.CustomPacket
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.api.plugin.Event

data class BPacketEvent(val from: ServerInfo, val packet: CustomPacket) : Event()