package com.bilibili.serverteleporter.listener

import com.bilibili.serverteleporter.api.events.BPacketEvent
import com.bilibili.serverteleporter.bungeeChannel
import com.bilibili.serverteleporter.logger
import com.bilibili.serverteleporter.network.PacketManager
import com.bilibili.serverteleporter.network.packets.PortalTeleportPacket
import com.bilibili.serverteleporter.pluginScope
import com.bilibili.serverteleporter.settings.BungeeSettings
import kotlinx.coroutines.launch
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.connection.Server
import net.md_5.bungee.api.event.PluginMessageEvent
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.event.EventHandler
import java.util.concurrent.atomic.AtomicReference

object BCMessageListener : Listener {
    @EventHandler
    fun onMessage(event: PluginMessageEvent) {
        val customPacket = PacketManager.getPacketByChannel(event.tag) ?: return
        val server = event.sender.also { if (it !is Server) return } as Server
        customPacket.read(AtomicReference(event.data))
        logger.verbose("接收到数据包: $customPacket")
        ProxyServer.getInstance().pluginManager.callEvent(BPacketEvent(server.info, customPacket))
    }

    @EventHandler
    fun onPacket(e: BPacketEvent) {
        val correspondenceServer =
            BungeeSettings.getCorrespondenceServer(e.from) ?: return logger.error("接收到来的${e.from}错误的数据包, 对应服务器是否忘记配置了?")
        pluginScope.launch {
            if (e.packet is PortalTeleportPacket) {
                val player = ProxyServer.getInstance().getPlayer(e.packet.player)
                player.sendMessage(TextComponent("§b| §7正在将你传送至对应服务器."))
                player.connect(correspondenceServer) { result, error ->
                    if (error != null) return@connect
                    if (!result) return@connect
                    pluginScope.launch {
                        bungeeChannel.send(correspondenceServer to e.packet)
                    }
                }
                return@launch
            }
            bungeeChannel.send(correspondenceServer to e.packet)
        }
    }
}