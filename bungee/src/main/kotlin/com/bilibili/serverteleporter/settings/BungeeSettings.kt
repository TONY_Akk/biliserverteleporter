package com.bilibili.serverteleporter.settings

import com.bilibili.serverteleporter.ServerTeleporterBungee
import com.bilibili.serverteleporter.logger
import com.google.common.collect.HashBiMap
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.config.Configuration
import net.md_5.bungee.config.ConfigurationProvider
import net.md_5.bungee.config.YamlConfiguration
import org.yaml.snakeyaml.error.YAMLException
import java.io.File
import java.nio.file.Files

object BungeeSettings {
    private val serverMap = HashBiMap.create<ServerInfo, ServerInfo>()

    private val settingsFile = File(ServerTeleporterBungee.instance.dataFolder, "settings.yml").apply {
        if (!exists()) {
            parentFile.mkdirs()
            Files.copy(ServerTeleporterBungee.instance.getResourceAsStream("settings.yml"), this.toPath())
        }
    }
    lateinit var settings: Configuration
        private set

    init {
        try {
            settings = ConfigurationProvider.getProvider(YamlConfiguration::class.java).load(settingsFile)
            val section = settings.getSection("correspondence")
            val mapped = section.keys
                .associateWith { key -> section.getString(key) }
                .map {
                    ProxyServer.getInstance().getServerInfo(it.key) to ProxyServer.getInstance().getServerInfo(it.value)
                }
            serverMap.putAll(mapped)
        } catch (e: YAMLException) {
            logger.error("§c加载配置文件 ${settingsFile.path} 时出现错误: §b${e.message}")
        }
    }

    fun getCorrespondenceServer(server: ServerInfo): ServerInfo? {
        return serverMap[server] ?: serverMap.inverse()[server]
    }
}