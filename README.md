# BiliServerTeleporter

一款允许玩家在BC的子服间建立自定义传送门的插件.

## 命令 & 权限

主命令: /serverteleporter 简写 /stper

### 子命令

/stper list 显示所有传送门 - 权限: stper.list (默认拥有)

/stper near 显示离玩家最近的传送门 (500格之内) - 权限: stper.near (默认拥有)

/stper remove <id> 移除传送门 - 权限: stper.remove (默认OP拥有)

### 其他权限

stper.portal_count.数字 - 玩家传送门上限

## 原理及具体实现

### 消息通信

消息通信通过Minecraft提供的MessageChannel实现.本插件将消息封装为数据包(`CustomPacket`), 写common的部分, 通过ByteArray进行读取.

消息所使用的频道: `teleporter:数据包ID`

所使用的数据包:

- ~~PortalCreatePacket~~
- PortalTeleportPacket
- PortalDestroyPacket
- ~~PortalCreatedPacket~~

### 传送门建立

服务器结构: BC, 在BC配置文件内预先配置好对应的子服A, B.

过程: 玩家从 A 服务器触发 `BlockIgniteEvent`, 通过方法获取与之临近的原木方块并对传送门结构进行判定. 如果达成判定条件则触发 `PortalPreCreateEvent`. 事件触发后玩家将会打开一个告示牌UI,
可以在其中对传送门的ID进行设置. ID设置完成后, 传送门在 A 服务器创建完成, 触发 `PortalPostCreateEvent`. 当玩家使用传送门向 B 服务器传送时, B 服务器在收到数据包后,
在地面的随机位置生成一个传送门与之对应, 并记录在配置文件内, 创建成功.

## 配置文件

### 共用配置

```yaml
#
# 共用配置文件
#

# 日志打印等级
# 可用值: VERBOSE, FINEST, FINE, INFO, WARN, ERROR, FATAL
loggerLevel: FINEST


#
# Bukkit 端配置文件 (BungeeCord 端无需修改此部分配置文件)
#

# 数据包发出无响应多长时间后重新发送
# 单位 毫秒
timeOut: 3000

#
# BungeeCord 端配置文件 (Bukkit 端无需修改此部分配置文件)
#

# 子服间对应关系
correspondence:
  s1: s2
```

### Bukkit

#### data.yml (传送门存储)

该文件由插件自动生成, 如无特殊需要请勿修改

```yaml
storagedPortals:
  - creator: TONY_All
    id: 114514
    axis: X
    height: 3
    weight: 2
  	corner: 
      ==: Location
      world: world
      x: 1
      y: 1
      z: 1
```
