plugins {
    kotlin("jvm") version "1.5.21"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "com.bilibili.serverteleporter"
version = "1.0.1"

allprojects {
    version = rootProject.version
    apply {
        plugin("kotlin")
        plugin(com.github.jengelman.gradle.plugins.shadow.ShadowPlugin::class.java)
    }

    repositories {
        mavenCentral()
        maven("https://hub.spigotmc.org/nexus/content/groups/public/")
    }

    dependencies {
        implementation(kotlin("stdlib"))
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")
        implementation("org.jetbrains:annotations:21.0.1")
    }

    tasks.withType<ProcessResources> {
        expand("version" to version)
    }

    tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
        archiveBaseName.set(rootProject.name)
        archiveClassifier.set("")
        if (project != rootProject) {
            archiveAppendix.set(project.name)
        }
    }
}

